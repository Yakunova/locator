package com.example.test2.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.test2.R;
import com.example.test2.model.Parks;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: HP
 * Date: 24.09.13
 * Time: 15:33
 * To change this template use File | Settings | File Templates.
 */
public class ParkArrayAdapter extends ArrayAdapter<Parks>
{
    private final Context context;
    private final ArrayList<Parks> values;
    public ParkArrayAdapter(Context context, ArrayList<Parks> values)
    {
        super(context, R.layout.park_item, values);
        this.context = context;
        this.values = values;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;

        if (convertView!=null)
        {
            rowView = convertView;
        }
        else
        {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.park_item, parent, false);
        }
        TextView discipline = (TextView) rowView.findViewById(R.id.Name);
        LinearLayout root = (LinearLayout) rowView.findViewById(R.id.root);

        discipline.setText(values.get(position).name);

        return rowView;
    }
}
