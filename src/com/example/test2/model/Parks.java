package com.example.test2.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.test2.db.DbOpenHelper;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Администратор
 * Date: 23.11.13
 * Time: 20:58
 * To change this template use File | Settings | File Templates.
 */
public class Parks
{
    public String uid;
    public String id;
    public String name;
    public Parks(String uid, String id, String name){
        this.uid = uid;
        this.id = id;
        this.name = name;
    }
    public void save(Context context){
        DbOpenHelper dbOpenHelper = new DbOpenHelper(context);
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("uid",this.uid);
        cv.put("id",this.id);
        cv.put("name",this.name);
        if (exists(db, String.valueOf(this.id)))
        {
            db.update("Parks", cv, "uid=?", new String[]{String.valueOf(this.uid)} );
        }
        else
        {
            db.insert("Parks",null,cv);
        }
        db.close();
    }
    private boolean exists(SQLiteDatabase db, String uid){
        Cursor cur = db.rawQuery("select * from Parks where id=? limit 1", new String[]{uid});
        return cur.getCount() != 0;
    }
    public void delete(Context context){
        DbOpenHelper dbOpenHelper = new DbOpenHelper(context);
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        /*db.delete("Parks", "uid=?", new String[]{String.valueOf(this.uid)});*/

        db.close();
    }
    public static Parks get(Context context, String uid){
        DbOpenHelper dbOpenHelper = new DbOpenHelper(context);
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("select * from Parks where uid=? limit 1", new String[]{uid});
        cur.moveToNext();
        Parks result = new Parks(cur.getString(0),
                                 cur.getString(1),
                                 cur.getString(2));
        db.close();
        return result;
    }
    public static ArrayList<Parks> getAll(Context context){
        ArrayList<Parks> result = new ArrayList<Parks>();
        DbOpenHelper dbOpenHelper = new DbOpenHelper(context);
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
     //   db.delete("Parks", null,null);
        Cursor cur = db.rawQuery("select * from Parks", null);
        if (cur.getCount()>0)
        {
            while(!cur.isLast())
            {
                cur.moveToNext();
                result.add(new Parks( cur.getString(0),
                        cur.getString(1), cur.getString(2) )
                );
            }
        }
        if (result.isEmpty())
        {
            //result = result;
        }
        db.close();
        return result;
    }


}
