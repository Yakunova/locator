package com.example.test2.activities;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.test2.R;
import com.example.test2.model.Parks;

import java.util.ArrayList;
public class MainListActivity extends ListActivity {
    private TextView myText;
    /*String[] names = { "iPad черненький", "iPad беленький",
            "iPad-ова шкурка", "iPad-оноска", "iPad-окрышка",
            "iPad-опечаталка","iPad черненький", "iPad беленький",
            "iPad-ова шкурка", "iPad-оноска", "iPad-окрышка",
            "iPad-опечаталка"}*/;
    //String[] array = Parks.getAll(this).toArray();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_list_activity);
        //Создаем ArrayAdapter
        ArrayList<Parks> parks = Parks.getAll(this);
        ArrayList<String> parksList = new ArrayList<String>();
        ArrayList<String> parksListUid = new ArrayList<String>();
        for (Parks park: parks)
        {
            parksList.add(park.name);
            parksListUid.add(park.uid);
        }
        String[] parksStrings = new String[parksList.size()];
        parksStrings = (String[])parksList.toArray(parksStrings);
        ArrayAdapter<String> myArrAd = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, parksStrings);
        setListAdapter(myArrAd);
    }
    public void onListItemClick(ListView parent, View v, int position, long id){
        Toast toast = Toast.makeText(getApplicationContext(), "Выбрана строка иномер "+position, Toast.LENGTH_LONG);
        toast.show();



    }
}
