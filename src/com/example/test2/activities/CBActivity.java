package com.example.test2.activities;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import com.example.test2.R;
import com.example.test2.model.Parks;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

public class CBActivity extends Activity {
//    private int NodeListLength;
//    private String Response;
    private String s;
    private ArrayList<Parks> sq = new ArrayList<Parks>();
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings );
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            String envelope="<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:loc=\"http://localhost/location\">" +
            "<soap:Header/>"+
            "<soap:Body>"+
            "<loc:GetFoundationTable/>"+
            "</soap:Body>"+
            "</soap:Envelope>";
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(new StringReader( CallWebService(URL,SOAP_ACTION,envelope))));
            // нормализируем документ
            doc.getDocumentElement().normalize();
            ////////////////////////////////////////
            // получаем корневой узел
            NodeList nodeList = doc.getElementsByTagName("m:return");
            Node node=nodeList.item(0);
            for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                // foundtaioninfos
                Node temp = node.getChildNodes().item(i);
                if (temp.getNodeName().equalsIgnoreCase("m:FoundationInfos")) {
                    for (int j = 0; j < temp.getChildNodes().getLength(); j++) {
                        Node found = temp.getChildNodes().item(j);
                        if(found.getNodeName().equalsIgnoreCase("m:Foundation")){
                            s = found.getTextContent();
                            Parks newPark = new Parks("","","");
                            for (int k = 0; k < found.getChildNodes().getLength(); k++) {
                                Node s = found.getChildNodes().item(k);
                                if(s.getNodeName().equalsIgnoreCase("m:Name")){
                                    newPark.name = s.getTextContent();
                                }
                                if(s.getNodeName().equalsIgnoreCase("m:UID")){
                                    newPark.uid = s.getTextContent();
                                }
                                if(s.getNodeName().equalsIgnoreCase("m:ID")){
                                    newPark.id = s.getTextContent();
                                }
                            }
                            sq.add(newPark);
                            newPark.save(this);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        onBackPressed();
    }
    private static final String SOAP_ACTION = "http://simplelogic.cloudapp.net/parks/ws/MobileExchange:GetFoundationTable";
    private static final String URL = "http://simplelogic.cloudapp.net/parks/ws/MobileExchange.1cws/";
    String CallWebService(String url,String soapAction,String envelope)  {
        final DefaultHttpClient httpClient=new DefaultHttpClient();
        // параметры запроса
        HttpParams params = httpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, 10000);
        HttpConnectionParams.setSoTimeout(params, 15000);
        // устанавливаем параметры
        HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);

        // С помощью метода POST отправляем конверт
        HttpPost httppost = new HttpPost(url);
        // и заголовки
        httppost.setHeader("soapaction", soapAction);
        httppost.setHeader("Content-Type", "text/xml; charset=utf-8");

        String responseString="";
        try {

            // выполняем запрос
            HttpEntity entity = new StringEntity(envelope);
            httppost.setEntity(entity);

            // Заголоаок запроса
            ResponseHandler<String> rh=new ResponseHandler<String>() {
                // вызывается, когда клиент пришлет ответ
                public String handleResponse(HttpResponse response)
                        throws ClientProtocolException, IOException {

                    // получаем ответ
                    HttpEntity entity = response.getEntity();

                    // читаем его в массив
                    StringBuffer out = new StringBuffer();
                    byte[] b = EntityUtils.toByteArray(entity);

                    // write the response byte array to a string buffer
                    out.append(new String(b, 0, b.length));
                    return out.toString();
                }
            };
            responseString=httpClient.execute(httppost, rh);
        }
        catch (Exception e) {
            Log.v("exception", e.toString());
        }
        // закрываем соединение
        httpClient.getConnectionManager().shutdown();

        return responseString;
    }
    @Override
    public Resources getResources() {
        return super.getResources();    //To change body of overridden methods use File | Settings | File Templates.
    }
}
