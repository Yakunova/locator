package com.example.test2.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebView;
import com.example.test2.R;
import com.example.test2.db.DbOpenHelper;
import com.example.test2.db.FileUtils;

public class MyActivity extends Activity
{
    private String string1 = "<!DOCTYPE html> \n" +
            "<html> \n" +
            "<head> \n" +
            " <title>Моя карта</title> \n" +
            " <script type=\"text/javascript\" src=\"http://maps.api.2gis.ru/1.0\"></script> \n" +
            " <script type=\"text/javascript\"> "+

    "  // Создаем обработчик загрузки страницы: \n"+
            "  DG.autoload(function() { \n"+
            "\n"+
            "   // Создаем объект карты, связанный с контейнером: \n"+
            "   var myMap = new DG.Map('myMapId'); \n"+
            "   // Устанавливаем центр карты, и коэффициент масштабирования: \n"+
            "   // Добавляем элемент управления коэффициентом масштабирования: \n"+
            "   myMap.controls.add(new DG.Controls.Zoom());\n"+
            "var myBalloon1 = new DG.Balloons.Common({\n"+
            " // Местоположение на которое указывает балун: \n"+
            " geoPoint: new DG.GeoPoint(37.599687538068,55.731636505784),\n"+
            " // Устанавливаем текст, который будет отображатся при открытии балуна:\n"+
            " contentHtml: 'Памятник'\n"+
            " });\n"+
            "var myBalloon2 = new DG.Balloons.Common({\n"+
            " // Местоположение на которое указывает балун: \n"+
            " geoPoint: new DG.GeoPoint(37.593330702703,55.724338194404),\n"+
            " // Устанавливаем текст, который будет отображатся при открытии балуна:\n"+
            " contentHtml: 'Ротонда'\n"+
            " });\n"+
            "var myMarker1 = new DG.Markers.Common({\n"+
            " // Местоположение на которое указывает маркер:\n"+
            " geoPoint: new DG.GeoPoint(37.599687538068,55.731636505784),   // Функция, вызываемая при клике по маркеру\n"+
            " clickCallback: function() {\n"+
            "  if (! myMap.balloons.getDefaultGroup().contains(myBalloon1)) {\n"+
            "   // Если балун еще не был добавлен на карту, добавляем его:\n"+
            "   myMap.balloons.add(myBalloon1);\n"+
            "  } else {\n"+
            "   // Показываем уже ранее добавленный на карту балун\n"+
            "   myBalloon1.show();\n"+
            "  }     \n"+
            " }\n"+
            "}); \n"+
            "var myMarker2 = new DG.Markers.Common({\n"+
            " // Местоположение на которое указывает маркер:\n"+
            " geoPoint: new DG.GeoPoint(37.593330702703,55.724338194404),   // Функция, вызываемая при клике по маркеру\n"+
            " clickCallback: function() {\n"+
            "  if (! myMap.balloons.getDefaultGroup().contains(myBalloon2)) {\n"+
            "   // Если балун еще не был добавлен на карту, добавляем его:\n"+
            "   myMap.balloons.add(myBalloon2);\n"+
            "  } else {\n"+
            "   // Показываем уже ранее добавленный на карту балун\n"+
            "   myBalloon2.show();\n"+
            "  }     \n"+
            " }\n"+
            "}); \n"+
            "myMap.markers.add(myMarker1)\n"+
            "myMap.markers.add(myMarker2)\n"+
            "// Получить границы маркеров:\n"+
            " var markersBounds = myMap.markers.getBounds();\n"+
            " // Устанавливаем карте новые границы по маркерам:\n"+
            " myMap.setBounds(markersBounds);\n"+
            " myMap.fullscreen.clickHandler();\n"+
            " myMap.fullscreen.disable();\n"+
            "}); \n"+
            " </script> \n"+
            "</head> \n"+
            "<body> \n"+
            " <div id=\"myMapId\" style=\"width:500px; height:400px\"></div> \n"+
            "</body> \n"+
            "</html>" ;
    private String string2 = "<!DOCTYPE html>\n" +
            "\n" +
            "<html>\n" +
            "<head>\n" +
            "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
            "    <title>Примеры. Размещение карты на странице.</title>\n" +
            "    <!--\n" +
            "        Подключаем API карт\n" +
            "        Параметры:\n" +
            "          - load=package.standard - основные компоненты;\n" +
            "       - lang=ru-RU - язык русский.\n" +
            "    -->\n" +
            "    <script src=\"http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU\" type=\"text/javascript\"></script>\n" +
            "\n" +
            "<script type=\"text/javascript\">\n" +
            "var myMap;\n" +
            "\n" +
            "// Дождёмся загрузки API и готовности DOM.\n" +
            "ymaps.ready(init);\n" +
            "\n" +
            "function init () {\n" +
            "    // Создание экземпляра карты и его привязка к контейнеру с\n" +
            "    // заданным id (\"map\").\n" +
            "    myMap = new ymaps.Map('map', {\n" +
            "        // При инициализации карты обязательно нужно указать\n" +
            "        // её центр и коэффициент масштабирования.\n" +
            "        center:[55.76, 37.64], // Москва\n" +
            "        zoom:10\n" +
            "    });\n" +
            "\n" +
            "    document.getElementById('destroyButton').onclick = function () {\n" +
            "        // Для уничтожения используется метод destroy.\n" +
            "        myMap.destroy();\n" +
            "    };\n" +
            "\n" +
            "}\n" +
            "</script>\n" +
            "</head>\n" +
            "\n" +
            "<body>\n" +
            "    <div id=\"map\" style=\"width:400px; height:300px\"></div>\n" +
            "    <input type=\"button\" id=\"destroyButton\" value=\"Удалить карту\"/>\n" +
            "</body>\n" +
            "\n" +
            "</html>";
    WebView mWebView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        createDb();

        mWebView = (WebView) findViewById(R.id.webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadDataWithBaseURL("http://www.example.com", string1, "text/html", "charset=UTF-8" , null);



        //TODO Взято из onOptionsItemSelected. Если все буднет работать тут, удалить оттуда.
        Intent intent = new Intent(MyActivity.this, CBActivity.class);
        startActivity(intent);
   }
    public void createDb(){
        DbOpenHelper dbOpenHelper = new DbOpenHelper(this);
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        String str [] = FileUtils.splitSqlFileToArray("/tablecreate.sql");
        for (String aStr : str) {
            db.execSQL(aStr);
        }
        db.close();}
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId())
        {
            case R.id.menu_settings:
                Intent intent = new Intent(MyActivity.this, CBActivity.class);
                startActivity(intent);
                return true;
           case R.id.list_view:
               Intent intent1 = new Intent(MyActivity.this, MainListActivity.class);
               startActivity(intent1);
               return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
